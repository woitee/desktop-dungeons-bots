package cz.woitee.ddapiexamples;

import java.io.IOException;

import cz.woitee.ddframework.BotBase;

/**
 * A bot that explores all fields of a dungeon and then finishes.
 * Basically the simplest possible bot, that has some behavior.
 * 
 * 
 * @author Vojtech Cerny
 */

public class ExploreBot extends BotBase {
	public ExploreBot() throws IOException {
		// Set name for minimal GUI
		name = "Explore Bot";
	}

	@Override
	public void step() throws IOException {
		// Walk to the first unexplored point in the dungeon
		state.walkTo(
			state.getExplorationMoves().get(0)
		);
	}
	
	@Override
	public boolean isFinished() {
		// Finished when no exploration moves exists
		return state.getExplorationMoves().isEmpty();
	}
	
	// STATIC METHODS
	// The simplest way of running the bot
	
	public static void main(String[] args) throws IOException {
		new ExploreBot().loop();
	}
}
