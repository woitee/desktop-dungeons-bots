package cz.woitee.ddapiexamples;

import java.io.IOException;

import cz.woitee.ddframework.BotBase;

/**
 * A commented bot template with no behaviour,
 * that can be copied when designing a new bot.
 * 
 * @author Vojtech Cerny
 *
 */
public class EmptyBot extends BotBase {
	/* Feel free to add any initialization to the constructor
	 * Just beware that the bot is not yet connected to the game
	 * Variables such as step time, or dungeon beasts can be set here, however
	*/
	public EmptyBot() throws IOException {
		/* removes the default step time to run as fast as possible*/
		//millisBetween = 0;
		/* removes the default wait time between runs in a loop*/
		//millisBetweenRuns = 0;
		
		/* sets custom generated beasts*/
		//setGeneratedBeasts(Type.Goblin, Type.Goo, Type.Zombie);
		
		/* Set custom name for minimal GUI*/
		name = "Set my name!";
	}

	/*
	 * Called once after the bot is connected to the game,
	 * but before any other method.
	 * 
	 * Suitable for one-time setup when access to game is needed.
	 */
	@Override
	protected void firstRunStart() {
		
	}
	
	/*
	 * Called before every run.
	 * 
	 * Suitable e.g. for resetting the bot's internal state.
	 */
	@Override
	protected void runStart() {
		
	}
	
	/*
	 * Called after every run.
	 * 
	 * Suitable e.g. for evaluating the bots performance.
	 */
	@Override
	protected void runEnd() {
		
	}
	
	/*
	 * Should perform one action. Nothing breaks if two or more are used,
	 * but stepping at a predefined time (millisBetween), will not work properly. 
	 */
	@Override
	public void step() throws IOException {
		
	}
	
	/*
	 * This method is used by the BotBase to check, whether the bot wants to continue
	 * going.
	 * 
	 * Default implementation in BotBase just checks the "finished" variable,
	 * so if more convenient, delete this method, and set the "finished" variable
	 * instead.
	 */
	@Override
	public boolean isFinished() {
		return super.isFinished();
	}
	
	
	/* STATIC METHODS
	 * 
	 * The bot runner.
	 */
	public static void main(String[] args) throws IOException {
		EmptyBot bot = new EmptyBot();
		// Run the bot in an infinite loop
		bot.loop();
		// Or alternatively use "run" to run just once
		//bot.run
	}
	
}
