package cz.woitee.ddapiexamples;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.woitee.ddframework.*;
import cz.woitee.ddframework.Glyph.Type;
import cz.woitee.ddframework.Hero.Clazz;
import cz.woitee.ddframework.Hero.Race;
import cz.woitee.ddframework.actions.Attack;
import cz.woitee.ddframework.actions.DrinkPotion;
import cz.woitee.ddframework.actions.DrinkPotion.PotionKind;
import cz.woitee.ddframework.actions.UseGlyph;
import cz.woitee.ddframework.BotBase;

/**
 * A fully implemented, hand-made greedy bot,
 * that mainly attacks and casts fireballs.
 * 
 * The most sophisticated example in this package, basically a complete 
 * and fine-tuned greedy strategy.
 * @author Vojtech Cerny
 *
 */
public class FireballBot extends BotBase {
	static Random random = new Random();
	int bossKills = 0;
	double bestFitness = 0;
	
	public FireballBot() throws IOException {
		name = "Fireball Bot";
		
		millisBetween = 200;
		millisBetweenRuns = 1000;
		cheatingVision = true;
		heroRace = Race.Elf;
		heroClass = Clazz.Wizard;
		//Goblin, Meat Man, Warlock, Zombie (basic)
		//Goat, Gorgon, Serpent, Wraith (Tier 2 monsters)
		setGeneratedBeasts(Beast.Type.Goblin, Beast.Type.Meatman, Beast.Type.Warlock, Beast.Type.Zombie);
		//setGeneratedBeasts(Beast.Type.Goblin, Beast.Type.Meatman, Beast.Type.Warlock, Beast.Type.Zombie,
		//				   Beast.Type.Goat, Beast.Type.Gorgon, Beast.Type.Serpent, Beast.Type.Wraith);
	}
	
	/**
	 * Specifies how many Mana Potions may the bot use in a fight
	 * against a beast of specific level.
	 * 
	 * @param beastLvl Level of the beast.
	 * @return The amount of mana potions the bot may use.
	 */
	protected int canUseManaPotions(int beastLvl) {
		switch (beastLvl) {
		case 8:
			return 1;
		case 9:
			return 2;
		case 10:
			return 100;
		default:
			return 0;
		}
	}
	/**
	 * Specifies how many Health Potions may the bot use in a fight
	 * against a beast of specific level.
	 * 
	 * @param beastLvl Level of the beast.
	 * @return The amount of health potions the bot may use.
	 */
	protected int canUseHealthPotions(int beastLvl) {
		switch (beastLvl) {
		case 8:
			return 1;
		case 9:
			return 2;
		case 10:
			return 100;
		default:
			return 0;
		}
	}
	/**
	 * Returns whether the bot can deplete its Kill Protection buff
	 * against a beast of a certain level.
	 * 
	 * @param beastLvl The level of the beasts.
	 * @return Whether the hero may deplete Kill Protection.
	 */
	protected boolean canDepleteKillProtect(int beastLvl) {
		return beastLvl >= 9;
	}
	
	/**
	 * Returns a doable exploration move.
	 * @return
	 */
	protected Point getExplorationMove() {
		List<Point> moves = state.getExplorationMoves();
		return getRandom(moves);
	}
	/**
	 * Returns a pickable item.
	 * @return
	 */
	protected Pickup getPickableItem() {
		List<Pickup> pickups = state.getPickups();
		return getRandom(pickups);
	}
	/**
	 * Returns a pickable glyph.
	 * @return
	 */
	protected Glyph getPickableGlyph() {
		boolean heroHasFreeSpot = false;
		for (Glyph heroGlyph : state.getHero().getGlyphs()) {
			if (heroGlyph == null) {
				heroHasFreeSpot = true;
				break;
			}
		}
		
		if (!heroHasFreeSpot)
			return null;
		
		List<Glyph> glyphs = state.getGlyphs();
		return getRandom(glyphs);
	}
	/**
	 * Tries to calculate a KillPlan - a sequence of actions to kill a beast
	 * of as high level as possible.
	 * 
	 * @return A sequence of actions to kill a beast and survive, or null,
	 * 			if none was found.
	 * @throws IOException
	 */
	protected List<DDAction> getKillPlan() throws IOException {
		List<DDAction> bestKillPlan = null;
		Beast strongestKillable = null;
		
		// Try to find a KillPlan for each beast
		// and return the one for the strongest (highest level)
		for (Beast beast : state.getBeasts()) {
			List<DDAction> killPlan = getKillPlan(beast);
			
			if (killPlan != null &&
			   (strongestKillable == null || strongestKillable.getLevel() < beast.getLevel())
			   ) {
				bestKillPlan = killPlan;
			}
		}
		
		return bestKillPlan;
	}
	/**
	 * This method tries to calculate a KillPlan - a sequence of actions to kill
	 * for a specific beast.
	 * 
	 * @param beast The beast we're trying to kill.
	 * @return
	 * @throws IOException
	 */
	protected List<DDAction> getKillPlan(Beast beast) throws IOException {
		Glyph heroFireball = state.getHero().getGlyphOfType(Type.BURNDAYRAZ);
		
		// Detach state for simulation of attacks, fireballs, etc.
		Beast detachedBeast = (Beast)beast.clone();
		Hero detachedHero = (Hero)state.getHero().clone();
		
		List<DDAction> tempPlan = new ArrayList<DDAction>();
		
		// Try casting fireball !
		if (heroFireball != null) {
			while (detachedHero.canCast(heroFireball)) {
				tempPlan.add(new UseGlyph(heroFireball, beast));
				detachedHero.performFireball(detachedBeast);
				
				if (detachedBeast.isDead()) {
					// Suceeded killing beast with only fireballs
					return tempPlan;
				}
			}
		}
		// Try simulating attacks!
		// We need another detached state to revert action if we die
		Hero detached2Hero = (Hero)detachedHero.clone();
		Beast detached2Beast = (Beast)detachedBeast.clone();
		boolean hadKillProtect = detachedHero.hasKillProtect();
		while (true) {
			detached2Hero.performAttack(detached2Beast);
			if 	(detached2Hero.isDead() ||
				(hadKillProtect && !detached2Hero.hasKillProtect())
				) {
				break;
			}
			
			// The hero made this attack without dying or using kill protection
			detachedHero.performAttack(detachedBeast);
			tempPlan.add(new Attack(beast));
			if (detachedBeast.isDead()) {
				return tempPlan;
			}
		}
		// Use as many Mana Potions as allowed, consume mana by casting fireballs
		int potions = canUseManaPotions(beast.getLevel());
		while (potions > 0 && detachedHero.getMPotions() > 0 && heroFireball != null) {
			--potions;
			detachedHero.drinkManaPotion();
			tempPlan.add(new DrinkPotion(PotionKind.MANA));
			
			while (detachedHero.canCast(heroFireball)) {
				tempPlan.add(new UseGlyph(heroFireball, beast));
				detachedHero.performFireball(detachedBeast);
				
				if (detachedBeast.isDead()) {
					// Suceeded killing beast with only fireballs
					return tempPlan;
				}
			}
		}
		// Use as many Health Potions as allowed, consume life by attacking
		potions = canUseHealthPotions(beast.getLevel());
		while (potions > 0 && detachedHero.getHPotions() > 0) {
			--potions;
			detachedHero.drinkHealthPotion();
			tempPlan.add(new DrinkPotion(PotionKind.HEALTH));
			
			hadKillProtect = detachedHero.hasKillProtect();
			detached2Hero = (Hero)detachedHero.clone();
			detached2Beast = (Beast)detachedBeast.clone();
			hadKillProtect = detachedHero.hasKillProtect();
			while (true) {
				detached2Hero.performAttack(detached2Beast);
				if 	(detached2Hero.isDead() ||
					(hadKillProtect && !detached2Hero.hasKillProtect())
					) {
					break;
				}
				
				// The hero made this attack without dying or using kill protection
				detachedHero.performAttack(detachedBeast);
				tempPlan.add(new Attack(beast));
				if (detachedBeast.isDead()) {
					return tempPlan;
				}
			}
		}
		// Attack using kill protect, if allowed
		if (canDepleteKillProtect(beast.getLevel()) && detachedHero.hasKillProtect()) {
			detachedHero.performAttack(detachedBeast);
			
			if (!detachedHero.isDead() && detachedBeast.isDead()) {
				tempPlan.add(new Attack(beast));
				return tempPlan;
			}
		}
		
		return null;
	}
	
	/**
	 * Pickup a glyph. The only one we use is BURNDAYRAZ, so convert the others.
	 * 
	 * @param glyph The glyph to pick up.
	 * @throws IOException
	 */
	protected void pickupAndMaybeConvert(Glyph glyph) throws IOException {
		Glyph.Type gType = glyph.getGlyphType();

		state.pickupGlyph(glyph);
		if (gType != Type.BURNDAYRAZ) {
			state.convertGlyph(glyph);
		}
	}
	/**
	 * Kill a beast by performing the found KillPlan.
	 * @param killPlan
	 * @throws IOException
	 */
	protected void killBeast(List<DDAction> killPlan) throws IOException {
		for (DDAction action : killPlan) {
			tryThreadSleep(millisBetween);
			state.doAction(action);
		}
	}
	
	@Override
	protected void runStart() {
		// An example usage of setting custom dungeon level
		// First run sets the current objects as permanent dungeon
		if (getRuns() == 1) {
			try {
				api.setDungeonLayout(state.getObjects());
			} catch (IOException e) {}
		}
		// Second run unsets it back to generated
		if (getRuns() == 2) {
			try {
				System.out.println("RESETTING");
				api.resetDungeonLayout();
			} catch (IOException e) {}
		}
		
		// This results in first two runss being in exactly the same dungeon
	}
	
	public void step() throws IOException {
		Point move = getExplorationMove();
		Pickup pickable = getPickableItem();
		List<DDAction> killPlan = getKillPlan();
		Glyph glyph = getPickableGlyph();
		
		boolean hasAction = move != null || pickable != null ||
							killPlan != null || glyph != null;
		
		if (!hasAction) {
			finished = true;
			return;
		}
			
		@SuppressWarnings("unused") // used for debugging
		String lastAction = "";
			
		if (pickable != null) {
			lastAction = "PICKUP " + pickable;
			state.pickup(pickable);
		} else if (glyph != null) {
			lastAction = "PICKUP " + glyph;
			pickupAndMaybeConvert(glyph);
		} else if (killPlan != null) {
			// killing is checked inside
			lastAction = "KILL BEAST Plan:\n" + killPlan;
			killBeast(killPlan);
		} else {
			// move cannot be null here, because hasAction is true
			lastAction = "MOVE " + move;
			state.walkTo(move);
		}
	} 
	
	// Fitness evaluation, feel free to ignore
	private static double evaluateState(DDState state) {
		Hero hero = state.getHero();
		if (hero.isDead())
			return -0.5;
		
		int collectedXP = 0;
		for (int i = 1; i < hero.getLevel(); ++i) {
			collectedXP += 5 * i;
		}
		collectedXP += hero.getXP();
		
		double fitness = collectedXP * 10 +
				hero.getHealth() * 1 +
				hero.getHPotions() * 150 +
				hero.getMPotions() * 75;
		
		if (state.isBossSlain()) {
			fitness *= 3;
		}
		
		return fitness;
	}
	
	// We evaluate fitness at the end of every run
	@Override
	protected void runEnd() {
		if (state.isBossSlain()) {
			++bossKills;
		}
		double fitness = evaluateState(state);
		if (fitness > bestFitness)
			bestFitness = fitness;
		
		
		System.out.println("Fitness of bot: " + fitness);
		System.out.println("Success rate: " + bossKills + "/" + getRuns());
		if (getRuns() % 10 == 0) {
			System.out.println("Best fitness found: " + bestFitness);
		}
	}
	
	// ==============
	// STATIC METHODS
	// ==============
	
	// Helper methods
	
	private static <T> T getRandom(List<T> list) {
		if (list.isEmpty()) {
			return null;
		}
		return list.get(random.nextInt(list.size()));
	}
	private static void tryThreadSleep(long millis) {
		try { 
			Thread.sleep(millis);
		} catch (Exception e) {}
	}
	
	public static void main(String[] args) throws IOException {
		FireballBot bot = new FireballBot();
		bot.loop();
	}
}
