package cz.woitee.ddapiexamples;

import java.awt.Point;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import cz.woitee.ddframework.Beast;
import cz.woitee.ddframework.BotBase;
import cz.woitee.ddframework.DDState;
import cz.woitee.ddframework.Glyph;
import cz.woitee.ddframework.Hero;
import cz.woitee.ddframework.Pickup;
import cz.woitee.ddframework.Glyph.Type;

/**
 * A bot employing a slightly different tactic, trying to exploit the BYSSEPS
 * glyph by using it as often as possible.
 * 
 * This example makes use of the game's simulation to find very simple plans
 * to kill monsters. These sections are thoroughly commented.
 * 
 * @author Vojtech Cerny
 *
 */
public class ByssepsBot extends BotBase {
	/**
	 * A kill plan containing the number of normal and bysseps-empowered
	 * attacks to kill a beast.
	 *
	 * @author Vojtech Cerny
	 *
	 */
	protected class KillPlan {
		Beast target;
		int normalAttacks;
		int byssepsAttacks;
		public KillPlan(Beast target, int byssepsAttacks, int normalAttacks) {
			this.target = target;
			this.byssepsAttacks = byssepsAttacks;
			this.normalAttacks = normalAttacks;
		}
	}
	
	Random random = new Random();
	
	protected boolean finished;
	
	public ByssepsBot() throws IOException {
		name = "Bysseps Bot";
	}
	
	public static void tryThreadSleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {}
	}
	
	/**
	 * Gets a random exploration move.
	 * @return
	 */
	protected Point getExplorationMove() {
		List<Point> moves = state.getExplorationMoves();
		if (moves.isEmpty()) {
			return null;
		}
		return moves.get(random.nextInt(moves.size()));
	}
	
	/**
	 * Gets a random pickable item.
	 * @return
	 */
	protected Pickup getPickableItem() {
		List<Pickup> pickups = state.getPickups();
		if (pickups.isEmpty()) {
			return null;
		}
		return pickups.get(random.nextInt(pickups.size()));
	}
	
	/**
	 * Gets a random pickable glyph.
	 * @return
	 */
	protected Glyph getPickableGlyph() {
		// We need to check whether the hero has room
		boolean heroHasFreeSpot = false;
		for (Glyph heroGlyph : state.getHero().getGlyphs()) {
			if (heroGlyph == null) {
				heroHasFreeSpot = true;
				break;
			}
		}
		if (!heroHasFreeSpot)
			return null;
		
		// Select random glyph from state
		List<Glyph> glyphs = state.getGlyphs();
		if (glyphs.isEmpty()) {
			return null;
		}
		return glyphs.get(random.nextInt(glyphs.size()));
	}
	
	/**
	 * Try to get a kill plan on a beast. That is, a number of bysseps attacks
	 * and a number of normal attacks to win a fight with a beast.
	 * 
	 * This method uses the game's simulation for simple search.
	 * 
	 * @return
	 * @throws IOException
	 */
	protected KillPlan getKillPlan() throws IOException {
		int attacks;
		int byssepses;
		Glyph heroBysseps = state.getHero().getGlyphOfType(Type.BYSSEPS);
		
		// Start creating attack plan for each beast
		List<Beast> beasts = state.getBeasts();
		for (Beast beast : beasts) {
			attacks = 0; byssepses = 0;
			// Detach the state, so we can simulate attacks
			DDState detached = state.detach();
			// We need the beast and hero objects in the simulation state
			Beast detachedBeast = detached.getEquivalent(beast);
			Hero detachedHero = detached.getHero();
			
			// Simulate attacks until death of hero or beast
			while (!detachedHero.isDead()) {
				// If hero can use the bysseps glyph, use it
				if (heroBysseps != null && detachedHero.canCast(heroBysseps)) {
					detached.useGlyph(heroBysseps);
					++byssepses;
				}
				
				// Simulate attack
				detached.attack(detachedBeast);
				++attacks;
				if (detachedBeast.isDead() && !detachedHero.isDead()) {
					// We managed to simulate killing the beast without dying
					// Return that kill plan
					return new KillPlan(beast, byssepses, attacks - byssepses);
				}
			}
		}
		return null;
	}
	
	/**
	 * Kills the beast, because it knows how many times to attack with bysseps
	 * and how many times without.
	 * 
	 * @param killPlan The kill plan to kill the beast.
	 * @throws IOException
	 */
	protected void killBeast(KillPlan killPlan) throws IOException {
		Beast beast = killPlan.target;
		Glyph heroBysseps = state.getHero().getGlyphOfType(Type.BYSSEPS);
		// Attack with BYSSEPS, as the KillPlan specifies
		for (int i = 0; i < killPlan.byssepsAttacks; ++i) {
			state.useGlyph(heroBysseps);
			state.attack(beast);
			// We're doing more than one action in the step() method
			// Keep the action speed constant
			tryThreadSleep(millisBetween);
		}
		// Attack normally, as the KillPlan specifies
		for (int i = 0; i < killPlan.normalAttacks; ++i) {
			state.attack(beast);
			tryThreadSleep(millisBetween);
		}
	}
	
	/**
	 * Pick up a glyph, and since we don't need any other glyph than BYSSEPS,
	 * auto-convert the other.
	 * 
	 * @param glyph
	 * @throws IOException
	 */
	protected void pickupAndMaybeConvert(Glyph glyph) throws IOException {
		Glyph.Type gType = glyph.getGlyphType();

		state.pickupGlyph(glyph);
		if (gType != Type.BYSSEPS) {
			state.convertGlyph(glyph);
		}
	}
	
	@Override
	public void step() throws IOException {
		// Gather all considered actions
		Point move = getExplorationMove();
		Pickup pickable = getPickableItem();
		KillPlan killPlan = getKillPlan();
		Glyph glyph = getPickableGlyph();
		
		// Determine if the bot has an action to perform
		boolean hasAction = move != null || pickable != null ||
							killPlan != null || glyph != null;
		
		if (!hasAction) {
			finished = true;
			return;
		}
	
		String lastAction = "";
		
		// Perform the action with highest priority
		if (pickable != null) {
			lastAction = "PICKUP " + pickable;
			state.pickup(pickable);
		} else if (glyph != null) {
			lastAction = "PICKUP " + glyph;
			pickupAndMaybeConvert(glyph);
		} else if (killPlan != null) {
			lastAction = "KILL";
			killBeast(killPlan);
		} else {
			lastAction = "MOVE " + move;
			state.walkTo(move);
		}
		
		System.out.println("Did action " + lastAction);
	}
	
	
	// ==============
	// STATIC METHODS
	// ==============
	
	// The bot runner
	
	public static void main(String[] args) throws IOException {
		ByssepsBot bot = new ByssepsBot();
		bot.loop();
	}
}
