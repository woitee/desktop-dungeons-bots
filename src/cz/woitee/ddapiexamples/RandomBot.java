package cz.woitee.ddapiexamples;

import java.io.IOException;
import java.util.List;

import cz.woitee.ddframework.*;

/**
 * A simple example bot, one that does random actions.
 * 
 * @author Vojtech Cerny
 *
 */
public class RandomBot extends BotBase {
	
	public RandomBot() throws IOException {
		name = "Random Bot";
	}
	
	@Override
	public void step() throws IOException {
		// Get all game's actions
		List<DDAction> actions = state.getActions();
		if (actions.isEmpty()) {
			// No actions? Finish the run then
			finished = true;
			return;
		}
		// Pick an action at random
		DDAction action = MyUtils.getRandom(actions);
		
		System.out.println("Doing Action: " + action.toString());
		
		// Perform the action
		state.doAction(action);
	}
	
	@Override
	public boolean isFinished() {
		// The run is over either if we said so in step()
		// or if we are dead
		return finished || state.getHero().isDead();
	}
	
	// STATIC METHODS
	// The simplest way of running the bot
	
	public static void main(String[] args) throws IOException {
		RandomBot bot = new RandomBot();
		bot.loop();
	}
}
