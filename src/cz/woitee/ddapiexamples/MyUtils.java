package cz.woitee.ddapiexamples;

import java.util.List;
import java.util.Random;

public class MyUtils {
	public static Random rng = new Random();
	
	public static <T extends Comparable<T>> T clamp(T amount, T low, T high) {
		if (amount.compareTo(low) < 0) {
			return low;
		} else if (amount.compareTo(high) > 0) {
			return high;
		}
		return amount;
	}
	
	public static <T> T getRandom (List<T> list) {
		if (list.isEmpty())
			return null;
		
		return list.get(rng.nextInt(list.size()));
	}
}
